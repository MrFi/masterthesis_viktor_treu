# Description Pages, Question Pages and Submit Pages

#Only analyse the workers after filter
list_of_workerIds_exp1 <- exp1_data$workerId
list_of_workerIds_exp23 <- friends_exp2_3$workerId
list_of_filtered_workers <- c(list_of_workerIds_exp1,list_of_workerIds_exp23)
list_of_filtered_workers <- list_of_filtered_workers[!duplicated(list_of_filtered_workers)]
total_data <- sorted_data[matchAll(list_of_filtered_workers, sorted_data$workerId), ]

sapply(total_data, function(total_data) length(unique(total_data)))


question_responses<- total_data[ which(is.na(total_data$responses)=='FALSE'), ]
question_responses$responses <- as.character(question_responses$responses)

question_responses <-question_responses[matchAll(list_of_workerIds, question_responses$workerId), ]

question_responses <- question_responses %>% 
  mutate(responses = map(responses, ~ fromJSON(.) %>% as.data.frame())) %>% 
  unnest(responses)


####### Plot the confidence of the workers 
ggplot(question_responses, aes(x= Confidence,  group=experimentName)) + 
  geom_bar(aes(y = ..prop.., fill = factor(..x..)), stat="count") +
  geom_text(aes( label = scales::percent(..prop..),
                 y= ..prop.. ), stat= "count", vjust = -.5, size=7) +
  scale_fill_manual(name="Decision", 
                    labels = c("I don't know", "No","Yes"), 
                    values = c("#34495e","#d35400","#16a085")) +  
  labs(y = "Percent", fill="Decision") +
  scale_y_continuous(labels = scales::percent) + ggtitle("Worker's Confidence")+
  theme(text = element_text(size=20))

######### Plot the influence of nudging 
ggplot(question_responses, aes(x= Nudges,  group=experimentName)) + 
  geom_bar(aes(y = ..prop.., fill = factor(..x..)), stat="count") +
  geom_text(aes( label = scales::percent(..prop..),
                 y= ..prop.. ), stat= "count", vjust = -.5, size=7) +
  scale_fill_manual(name="Decision", 
                    labels = c("I don't know", "No", "Yes"), 
                    values = c("#34495e","#d35400","#16a085")) +
  labs(y = "Percent", fill="Decision") +
  scale_y_continuous(labels = scales::percent) + ggtitle("Effect of digital nudging")+
  theme(text = element_text(size=20))

######### Filter by submitPage to see the total time consumed by the whole experiment.
friends_final_time <- total_data[ which(total_data$experimentName=='submitPage'), ]