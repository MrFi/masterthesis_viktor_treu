Please install NodeJS before running the project. Also have a MongoDB instance ready. 
Change the connection to DB in app.js file.

Install npm dependencies with "npm install".

Start project with: "npm run dev"
