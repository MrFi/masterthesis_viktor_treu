import config

new_hit = config.client.create_hit(
    Title = config.TITLE ,
    Description = config.DESCRIPTION,
    Keywords = config.KEYWORDS,
    LifetimeInSeconds = config.LIFETIME,
    AssignmentDurationInSeconds = config.DURATION,    
    MaxAssignments = config.MAX_ASSIGNMENTS,
    Question = config.QUESTION,
    Reward = config.REWARD
)

print("A new HIT has been created. You can preview it here:")
print(config.PREVIEW_HOST + new_hit['HIT']['HITGroupId'])
print("HITID = " + new_hit['HIT']['HITId'] + " (Use to Get Results)")