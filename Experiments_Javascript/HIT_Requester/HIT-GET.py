import config
import xmltodict
from xml.dom.minidom import parseString



def get_answer_by_hit_id(hit_id):

   worker_results = config.client.list_assignments_for_hit(
         HITId = hit_id, 
         AssignmentStatuses = ['Submitted']
      )

   if worker_results['NumResults'] > 0:
      
      for assignment in worker_results['Assignments']:
         xml_doc = xmltodict.parse(assignment['Answer'])
         
         print ("Worker's answer was:")
         
         if type(xml_doc['QuestionFormAnswers']['Answer']) is list:
            
            for answer_field in xml_doc['QuestionFormAnswers']['Answer']:
               
               print("\n For input field: " + answer_field['QuestionIdentifier'])
               print("Submitted answer 1 : " + answer_field['FreeText'])
         
         else:
            
            print("\n For input field: " + xml_doc['QuestionFormAnswers']['Answer']['QuestionIdentifier'])
            print("Submitted answer 2 : " + xml_doc['QuestionFormAnswers']['Answer']['FreeText'])
   
   else:
      
      print("No results ready yet")

# get_answer_by_hit_id('31J7RYECZN1BM7OQM59ZB2B5188L1N')



def get_answer_by_hit_id_2(hit_id):

    hit = config.client.get_hit(HITId=hit_id)
    print('Hit {} status: {}'.format(hit_id, hit['HIT']['HITStatus']))

    response = config.client.list_assignments_for_hit(
        HITId=hit_id,
        AssignmentStatuses=['Submitted', 'Approved'],
        MaxResults=80,
    )

    assignments = response['Assignments']
    print('The number of submitted assignments is {}'.format(len(assignments)))

    for assignment in assignments:
        worker_id = assignment['WorkerId']
        assignment_id = assignment['AssignmentId']
        answer_xml = parseString(assignment['Answer'])
        assignment_status = assignment['AssignmentStatus']

        # the answer is an xml document. we pull out the value of the first
        # //QuestionFormAnswers/Answer/FreeText
        answer = answer_xml.getElementsByTagName('FreeText')[0]

        # See https://stackoverflow.com/questions/317413
        only_answer = " ".join(t.nodeValue for t in answer.childNodes if t.nodeType == t.TEXT_NODE)

        #print('\n')
        #print('The worker ID {}'.format(worker_id))
        #print('The assignment ID {}'.format(assignment_id))
        #print('The assignment status {}'.format(assignment_status))
        #print('The assignment answer {}'.format(only_answer))

get_answer_by_hit_id_2('3ICOHX7ENEMX9CTG90MI8CJOUVW0EV')