import config



def get_account_balance():
  print(config.client.get_account_balance()['AvailableBalance'])



def get_list_all_hit():

   for item in config.client.list_hits()['HITs']:
      
      print('\n')

      hit_id = item['HITId']
      print('HITId:', hit_id)
      
      hit_info = config.client.get_hit(HITId=hit_id)['HIT'] 
      # print('HITInfo:', hit_info)

      CreationTime = hit_info['CreationTime']
      print('CreationTime:', CreationTime)

      Expiration = hit_info['Expiration']
      print('Expiration:', Expiration)

      status = hit_info['HITStatus']
      print('HITStatus:', status)

      response = config.client.list_assignments_for_hit(
        HITId=hit_id,
        AssignmentStatuses=['Submitted', 'Approved'],
        MaxResults=10,
      )
      assignments = response['Assignments']
      print('The number of submitted assignments is {}'.format(len(assignments)))

      num_assignments_approved = 0
      for assignment in assignments:
         if assignment['AssignmentStatus'] == 'Approved':
            num_assignments_approved += 1
      print('The number of submitted assignments is {}'.format(num_assignments_approved))



get_list_all_hit()
#get_account_balance()