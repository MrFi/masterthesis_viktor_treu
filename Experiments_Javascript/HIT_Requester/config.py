from boto.mturk.question import ExternalQuestion
import boto3



REGION_NAME = 'us-east-1'
AWS_ACCESS_KEY_ID = 'AKIAJG4JZVHXXUSWZZEA'
AWS_SECRET_ACCESS_KEY = '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'
DEV_ENVIROMENT_BOOLEAN = False
DEBUG = True

AMAZON_HOST_SANDBOX = 'https://mturk-requester-sandbox.us-east-1.amazonaws.com'
AMAZON_HOST_PROD = 'https://mturk-requester.us-east-1.amazonaws.com'

AMAZON_HOST = ""
if DEV_ENVIROMENT_BOOLEAN:
    AMAZON_HOST = AMAZON_HOST_SANDBOX
else:
    AMAZON_HOST = AMAZON_HOST_PROD



TITLE = "Comparison between choices made by artifical intelligence and human users"
DESCRIPTION = "Sentiment Analysis of movie reviews"
KEYWORDS = "Sentiment, Movie Reviews, Classification"

# An amount of time, in seconds, after which the HIT is no longer available for users to accept. 
LIFETIME = 240*60

# The amount of time, in seconds, that a Worker has to complete the HIT after accepting it.
DURATION = 30*60 # in seconds

# The number of times the HIT can be accepted and completed before the HIT becomes unavailable.
MAX_ASSIGNMENTS = 73

URL = "https://treu.masterthesis.tk/"
FRAME_HEIGHT = 700 # in pixel
QUESTION = ExternalQuestion(URL, FRAME_HEIGHT).get_as_xml()

# The amount of money the Requester will pay a Worker for successfully completing the HIT.
REWARD = '1.42'

# set requirement for worker
# qualifications = Qualifications()
# qualifications.add(PercentAssignmentsApprovedRequirement(comparator="GreaterThan", integer_value="90"))
# qualifications.add(NumberHitsApprovedRequirement(comparator="GreaterThan", integer_value="100"))
# QUALIFICATION = qualifications 



PREVIEW_HOST = ""
if DEV_ENVIROMENT_BOOLEAN:
    PREVIEW_HOST = "https://workersandbox.mturk.com/mturk/preview?groupId="
else:
    PREVIEW_HOST = "https://worker.mturk.com/mturk/preview?groupId="



client = boto3.client(
    'mturk',
    endpoint_url = AMAZON_HOST,
    region_name = REGION_NAME,
    aws_access_key_id = AWS_ACCESS_KEY_ID,
    aws_secret_access_key = AWS_SECRET_ACCESS_KEY
)