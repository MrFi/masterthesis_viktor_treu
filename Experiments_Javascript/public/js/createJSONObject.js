function createJSONObject() {
    let textStructure = '';
    for (let index = 0; index <= amount_text_per_experiment - 1; index++) {
        if (index === amount_text_per_experiment - 1) {
            textStructure +=
                "    {\n" +
                "      \"checkboxes" + index + "\": [\n" +
                "      ],\n" +
                "      \"labels" + index + "\": [\n" +
                "      ],\n" +
                "      \"state" + index + "\": [\n" +
                "      ],\n" +
                "      \"text" + index + "\": [\n" +
                "      ],\n" +
                "      \"movieId" + index + "\": [\n" +
                "      ],\n" +
                "      \"index" + index + "\": [\n" +
                "      ]\n" +
                "    }\n";
        } else {
            textStructure +=
                "    {\n" +
                "      \"checkboxes" + index + "\": [\n" +
                "      ],\n" +
                "      \"labels" + index + "\": [\n" +
                "      ],\n" +
                "      \"state" + index + "\": [\n" +
                "      ],\n" +
                "      \"text" + index + "\": [\n" +
                "      ],\n" +
                "      \"movieId" + index + "\": [\n" +
                "      ],\n" +
                "      \"index" + index + "\": [\n" +
                "      ]\n" +
                "    },\n"
        }
    }
    let grid = JSON.parse(
        "{\n" +
        "  \"Containers\": [\n" +
        textStructure +
        "  ]\n" +
        "}");
    return grid;
}

function httpGet(theUrl) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", theUrl, false); // false for synchronous request
    xmlHttp.send(null);
    return xmlHttp.responseText;
}

function editData(data, isHuman, concatData, stopValue) {
    let localData;
    if (concatData) {
        let localData_sort = data.sort(() => 0.5 - Math.random());
        localData = localData_sort.slice(0, stopValue);
        return localData;
    } else if (isHuman) {
        let localData_filter = data.filter(obj => obj.isHuman);
        localData = removeDuplicates(localData_filter, "movie_id")
    } else {
        let localData_filter = data.filter(obj => !obj.isHuman);
        localData = removeDuplicates(localData_filter, "movie_id")
    }
    localData = localData.sort(() => 0.5 - Math.random());
    localData = localData.slice(0, stopValue);
    return localData;
}

function deleteEntryFromArray(entries, oldData) {
    let usedReviews = [];
    let newData;
    entries.forEach(element => usedReviews.push(element.movie_id));
    let toDelete = new Set(usedReviews);
    newData = oldData.filter(obj => !toDelete.has(obj.movie_id));
    return newData
}

let postToAmazon = function (path, params, method = 'post') {
    const form = document.createElement('form');
    form.method = method;
    form.action = path;

    for (const key in params) {
        if (params.hasOwnProperty(key)) {
            const hiddenField = document.createElement('input');
            hiddenField.type = 'hidden';
            hiddenField.name = key;
            hiddenField.value = params[key];

            form.appendChild(hiddenField);
        }
    }

    document.body.appendChild(form);
    form.submit();
};

let getUrlParameter = function (sParam) {
    let sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? null : decodeURIComponent(sParameterName[1]);
        }
    }
    return null;
};

let pushToDatabase = function (url, method, data) {
    fetch(url, {
        method: method, // *GET, POST, PUT, DELETE, etc.
        headers: {
            'Content-Type': 'application/json'
            // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: JSON.stringify(data)// body data type must match "Content-Type" header
    })
        .then(function (response) {
            if (response.ok) {
                console.log("DONE");
                return;
            }
            throw new Error('Request failed.');
        })
        .catch(function (error) {
            console.log(error);
        });
};

let pushBackup = function (data, assignmentId, workerId, hitId, experimentName, currentNode, checkValue) {
    data.assignmentId = assignmentId;
    data.workerId = workerId;
    data.hitId = hitId;
    data.experimentName = experimentName;
    let data_from_current_node = jsPsych.data.getDataByTimelineNode(currentNode).ignore('stimulus').values();
    final_data_jsPsych.push(data_from_current_node[0]);
    if (checkValue) {
        pushToDatabase('/humanReview', 'POST', data_from_current_node[0]);
    }
    pushToDatabase('/backup', 'POST', data_from_current_node[0]);
};

function removeDuplicates(data, field) {
    return data.filter((obj, pos, arr) => {
        return arr.map(mapObj => mapObj[field]).indexOf(obj[field]) === pos;
    });
}