const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const mongo = require('mongodb');
const _ = require('lodash');

const MongoClient = mongo.MongoClient;
// const uri = "mongodb+srv://default_user:Masterthesis@turingtest-bcxak.mongodb.net/test?retryWrites=true&w=majority";
const uri = "mongodb+srv://default_user:Masterthesis@turingtest.bcxak.mongodb.net//test?retryWrites=true&w=majority";
let client;

const mongoClient = new MongoClient(uri, {
    reconnectTries:
    Number.MAX_VALUE, autoReconnect: true, useNewUrlParser: true
});
mongoClient.connect((err, db) => { // returns db connection
    if (err != null) {
        console.log(err);
        return
    }
    client = db;
    console.log("MongoCluster connected!")
});

app.use(bodyParser.json({extended: true}));
app.use(express.static(__dirname + '/public'));
app.get('/', (req, res) => {
    res.sendFile(__dirname + 'index.html');
});

app.get('/text', (req, res) => {
    const collection = client.db("Experiment").collection("TestData");
    collection.aggregate(
        [
            {
                $match: {isHuman: {$exists: false}}
            },
            {
                $sample: {size: 1}
            }
        ]).toArray(function (err, result) {
        if (err) return console.log(err);
        res.send(result);
    })
});

app.post('/humanReview', (req, res) => {
    const data = req.body;
    const collection = client.db("Experiment").collection("TestData");

    collection.save(data, (err, result) => {
        if (err) {
            return console.log(err);
        }
        res.sendStatus(201);
    });
});

app.post('/results', (req, res) => {
    const data = req.body;
    console.log(data);
    const collection = client.db("Experiment").collection("Results");

    collection.insertMany(data, (err, result) => {
        if (err) {
            return console.log(err);
        }
        res.sendStatus(201);
    });
});

app.post('/backup', (req, res) => {
    const data = req.body;
    console.log(data);
    const collection = client.db("Experiment").collection("BackupData");

    collection.save(data, (err, result) => {
        if (err) {
            return console.log(err);
        }
        res.sendStatus(201);
    });
});

app.get('/get', async (req, res) => {
    const collection = client.db("Experiment").collection("TestData");
    const queries = [
        // collection.find({isHuman: {$exists: false}}).limit(5).toArray(),
        // collection.find({isHuman: true}).limit(5).toArray(),
        collection.find({}).toArray()
    ];
    const result = await Promise.all(queries);
    // const merged = _.shuffle(_.concat(result[0], result[1]));
    // const index = _.random(0, 9);
    res.send(result);
});

app.listen(3000, function () {
    console.log('listening on 3000')
});